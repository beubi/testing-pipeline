FROM microsoft/iis

RUN powershell -NoProfile -Command Remove-Item -Recurse C:\inetpub\wwwroot\*

RUN powershell Install-WindowsFeature NET-Framework-45-ASPNET ; Install-WindowsFeature Web-Asp-Net45


WORKDIR /windows/system32/inetsrv

RUN appcmd add app /site.name:"Default Web Site" /path:/server /physicalPath:c:\server

WORKDIR /
RUN mkdir server
COPY server/ /server